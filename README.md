## Описание
Terraform и ansible код создающий и настраивающий:
1. сервер для запуска docker образа (сервиса)
2. сервер для запуска Gitlab Runner
3. сервер Prometheus
4. сервер Grafana
5. Prometheus exporter-ы на нужных серверах
6. load balancer в Yandex-облаке
7. днс зону testsalad.tk

## Требования/установка
* terraform - https://learn.hashicorp.com/tutorials/terraform/install-cli
```
sudo apt-get update && sudo apt-get install -y gnupg software-properties-common
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
    sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update
sudo apt-get install terraform
```
* ansible - https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html
```
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3 get-pip.py --user
python3 -m pip install --user ansible
```


## Использование
* предварительная настройка
```
создать файл ansible/secret.vars , в котором задать переменную runner_reg_token с токеном регистрации для Gitlab Runner

в файле ansible/group_vars/all.yml можно отредактировать переменные, используемые различными ролями при конфигурации
```
* запуск
```
cd terraform
terraform init -backend-config=config.gitlab.tfbackend (пример файла конфигурации - terraform/config.gitlab.tfbackend.example)
terraform apply -auto-approve (автоматически запускает ansible-playbook)
```

