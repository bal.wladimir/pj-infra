terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
}

provider "yandex" {
  token     = var.yandex_token
  cloud_id  = var.yandex_cloud_id
  folder_id = var.yandex_folder_id
  zone      = "ru-central1-a"
}

module "vpc" {
  source = "../modules/vpc"
}

module "dns" {
  source = "../modules/dns"
}

resource "yandex_iam_service_account" "main" {
  name        = "sa-main"
  description = "service account to manage IG"
}

resource "yandex_resourcemanager_folder_iam_binding" "editor" {
  folder_id = var.yandex_folder_id
  role      = "editor"
  members = [
    "serviceAccount:${yandex_iam_service_account.main.id}",
  ]
}

resource "yandex_compute_instance_group" "prod" {
  name               = "fixed-ig-with-balancer-1"
  folder_id          = var.yandex_folder_id
  service_account_id = yandex_iam_service_account.main.id
  # prevents permission error when destroying  
  depends_on = [yandex_resourcemanager_folder_iam_binding.editor]
  instance_template {
    platform_id = "standard-v3"
    resources {
      memory        = 1
      cores         = 2
      core_fraction = 20
    }
    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = var.yandex_boot_image_id
        size     = 5
      }
    }
    network_interface {
      network_id = module.vpc.id
      subnet_ids = ["${module.vpc.ru-central1-a-id}", "${module.vpc.ru-central1-b-id}"]
      nat        = true
    }
    metadata = {
      ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
    }
    network_settings {
      type = "STANDARD"
    }
    scheduling_policy {
      preemptible = true
    }
  }

  scale_policy {
    fixed_scale {
      size = var.server_count
    }
  }
  allocation_policy {
    zones = ["ru-central1-a", "ru-central1-b"]
  }

  deploy_policy {
    max_unavailable = 1
    max_creating    = 1
    max_expansion   = 0
    max_deleting    = 1
  }

  load_balancer {
    target_group_name        = "ig-1-lb"
    target_group_description = "main load balancer ig-1-lb"
  }
}

resource "yandex_lb_network_load_balancer" "prod" {
  name = "ig-1-lb"
  listener {
    name = "ig-1-lb-listener"
    port = 8080
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_compute_instance_group.prod.load_balancer[0].target_group_id

    healthcheck {
      name = "http"
      http_options {
        port = 8080
      }
    }
  }
}

resource "yandex_dns_recordset" "test_testsalad_tk" {
  zone_id = module.dns.zone_id
  name    = "prod.testsalad.tk."
  type    = "A"
  ttl     = 200
  data    = ["${one(one(yandex_lb_network_load_balancer.prod.listener[*].external_address_spec[*].address))}"]
}

resource "yandex_dns_recordset" "monitoring_testsalad_tk" {
  zone_id = module.dns.zone_id
  name    = "monitoring.testsalad.tk."
  type    = "A"
  ttl     = 200
  data    = ["${one(yandex_compute_instance.monitoring.network_interface[*].nat_ip_address)}"]
}

resource "yandex_dns_recordset" "grafana_testsalad_tk" {
  zone_id = module.dns.zone_id
  name    = "grafana.testsalad.tk."
  type    = "A"
  ttl     = 200
  data    = ["${one(yandex_compute_instance.monitoring.network_interface[*].nat_ip_address)}"]
}

# create ansible inventory file and run playbook
resource "local_file" "hosts_prod" {
  content = templatefile("${path.root}/../templates/hosts.tpl",
    {
      host_group   = "servers"
      servers      = flatten(yandex_compute_instance_group.prod.instances[*].network_interface[*].nat_ip_address)
      ssh_pvt_path = var.ssh_pvt_path
    }
  )
  filename = "../../ansible/_hosts/hosts_prod"

  provisioner "remote-exec" {    
    inline = ["sudo apt update", "echo Done!"]
    
    connection {
      host        = one(flatten(yandex_compute_instance_group.prod.instances[*].network_interface[*].nat_ip_address))
      type        = "ssh"
      user        = "ubuntu"
      private_key = file(var.ssh_pvt_path)
    }
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ../../ansible/_hosts/hosts_prod -T 300 ../../ansible/main_playbook.yml"
  }
}

resource "yandex_compute_instance" "gitlab_runner" {
  name        = "gitlab-runner"
  platform_id = "standard-v3"
  zone        = "ru-central1-a"

  resources {
    memory        = 1
    cores         = 2
    core_fraction = 20
  }

  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = var.yandex_boot_image_id
      size     = 6
    }
  }

  network_interface {
    subnet_id = module.vpc.ru-central1-a-id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

resource "local_file" "hosts_runner" {
  content = templatefile("${path.root}/../templates/hosts.tpl",
    {
      host_group   = "servers"
      servers      = yandex_compute_instance.gitlab_runner.network_interface[*].nat_ip_address
      ssh_pvt_path = var.ssh_pvt_path
    }
  )
  filename = "../../ansible/_hosts/hosts_runner"

  provisioner "remote-exec" {    
    inline = ["sudo apt update", "echo Done!"]
    
    connection {
      host        = one(yandex_compute_instance.gitlab_runner.network_interface[*].nat_ip_address)
      type        = "ssh"
      user        = "ubuntu"
      private_key = file(var.ssh_pvt_path)
    }
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ../../ansible/_hosts/hosts_runner -T 300 ../../ansible/gitlab_runner_playbook.yml"
  }
}


resource "yandex_compute_instance" "monitoring" {
  name        = "monitoring-prometheus"
  platform_id = "standard-v3"
  zone        = "ru-central1-a"

  resources {
    memory        = 1
    cores         = 2
    core_fraction = 20
  }

  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = var.yandex_boot_image_id
      size     = 5
    }
  }

  network_interface {
    subnet_id = module.vpc.ru-central1-a-id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

resource "local_file" "hosts_monitoring" {  
  content = templatefile("${path.root}/../templates/hosts.tpl",
    {
      host_group   = "servers"
      servers      = yandex_compute_instance.monitoring.network_interface[*].nat_ip_address
      ssh_pvt_path = var.ssh_pvt_path
    }
  )
  filename = "../../ansible/_hosts/hosts_monitoring"

  provisioner "remote-exec" {    
    inline = ["sudo apt update", "echo Done!"]

    connection {
      host        = one(yandex_compute_instance.monitoring.network_interface[*].nat_ip_address)
      type        = "ssh"
      user        = "ubuntu"
      private_key = file(var.ssh_pvt_path)
    }
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ../../ansible/_hosts/hosts_monitoring -T 300 ../../ansible/monitoring_playbook.yml"
  }
}
