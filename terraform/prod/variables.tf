variable "yandex_token" {
  type      = string
  sensitive = true
}

variable "ssh_pub" {
  type = string
}

variable "ssh_pvt_path" {
  type = string
}

variable "yandex_cloud_id" {
  type = string
}

variable "yandex_folder_id" {
  type = string
}

variable "yandex_boot_image_id" {
  type = string
}


variable "remote_state_address" {
  type        = string
  description = "Gitlab remote state file address"
}

variable "gitlab_username" {
  type        = string
  description = "Gitlab username to query remote state"
}

variable "gitlab_terraform_access_token" {
  type        = string
  description = "GitLab access token to query remote state"
}

variable "server_count" {
  type = string
}

