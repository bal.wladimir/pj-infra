variable "yandex_token" {
  type      = string
  sensitive = true
}

variable "ssh_pub" {
  type = string
}

variable "ssh_pvt_path" {
  type = string
}

variable "yandex_cloud_id" {
  type = string
}

variable "yandex_folder_id" {
  type = string
}

variable "yandex_boot_image_id" {
  type = string
}

