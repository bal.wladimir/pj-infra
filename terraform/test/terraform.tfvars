yandex_cloud_id  = "b1g5kb47s1ftrsvl0fuo"
yandex_folder_id = "b1gbd7opign7umspm99b"
# https://cloud.yandex.ru/marketplace/products/yc/ubuntu-20-04-lts
yandex_boot_image_id = "fd8ju9iqf6g5bcq77jns"

ssh_pub              = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDVSi1XJGLkJUYWgiGYW8ZaE+jU98CBU5tK7/aEZOLMH+PxEgIOlo10897AgsSH4bKB/uHQFlTj+mwkLLwmZlwQoRTr5SPee1ivD1ud8y9OL5PN7b4N3p+0tGosiLOxeE36o1kk/v4w040OSyPpIlcrgzShUQ6Q/O5K79foISrL+rEgHSqFIGJ/StcNvxKGGC1fZfhYDKV6/XlismXeyfBT8rr9mLRtId0gVTxp0ajwvOjwsjMDLQqQtG1NF60YulfypJgIEvH+seM0Ssrq5bTiv8ZE1LicTRrFF3V67WTFRa0r8m8gMhG1z249vSISMFxnLQwxcQbDX9vl1gJ59W//62yrI4crkx7g6HIX/9XACLqIvR3WdydKg6w4BneRnSCQDWmAzeAGtuQpvdE1GBe99OQKZpgJi1ttkEiJdKloALy8IO+zCdGQkrP4ukbVm0NbPCbN+4Mp48tplMlGTYoQCZNTPeAJub8zB9gTohtoFw514l8IpDGud7iRc3xzTKU="
remote_state_address = "https://gitlab.com/api/v4/projects/38395635/terraform/state/default"
gitlab_username      = "bal.wladimir"

ssh_pvt_path = "~/.ssh/id_rsa"

server_count = 1