terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
}

provider "yandex" {
  token     = var.yandex_token
  cloud_id  = var.yandex_cloud_id
  folder_id = var.yandex_folder_id
  zone      = "ru-central1-a"
}

resource "yandex_compute_instance" "test" {
  name        = "test-server"
  platform_id = "standard-v3"
  zone        = "ru-central1-a"

  resources {
    memory        = 1
    cores         = 2
    core_fraction = 20
  }

  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = var.yandex_boot_image_id
      size     = 5
    }
  }

  network_interface {
    subnet_id = data.yandex_vpc_subnet.ru-central1-a.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

data "yandex_dns_zone" "public" {
  name = "public-testsalad-tk-zone"
}

data "yandex_vpc_subnet" "ru-central1-a" {
  name = "ru-central1-a subnet"
}

resource "yandex_dns_recordset" "test_testsalad_tk" {
  zone_id = data.yandex_dns_zone.public.id
  name    = "test.testsalad.tk."
  type    = "A"
  ttl     = 200
  data    = ["${one(yandex_compute_instance.test.network_interface[*].nat_ip_address)}"]
}


# create ansible inventory file and run playbook
resource "local_file" "hosts_test" {
  content = templatefile("${path.root}/../templates/hosts.tpl",
    {
      host_group   = "servers"
      servers      = yandex_compute_instance.test.network_interface[*].nat_ip_address
      ssh_pvt_path = var.ssh_pvt_path
    }
  )
  filename = "../../ansible/_hosts/hosts_test"

  provisioner "remote-exec" {    
    inline = ["sudo apt update", "echo Done!"]
    
    connection {
      host        = one(yandex_compute_instance.test.network_interface[*].nat_ip_address)
      type        = "ssh"
      user        = "ubuntu"
      private_key = file(var.ssh_pvt_path)
    }
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ../../ansible/_hosts/hosts_test -T 300 ../../ansible/main_playbook.yml"
  }
}
