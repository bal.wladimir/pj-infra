variable "vpc_cidr_a" {
    type = string
    default = "192.168.10.0/24"
}

variable "vpc_cidr_b" {
    type = string
    default = "192.168.11.0/24"
}