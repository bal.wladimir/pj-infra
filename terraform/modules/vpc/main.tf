terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
}

resource "yandex_vpc_network" "main" {
  name = "main vpc network"
}

resource "yandex_vpc_subnet" "ru-central1-a" {
  name           = "ru-central1-a subnet"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.main.id
  v4_cidr_blocks = [var.vpc_cidr_a]
}

resource "yandex_vpc_subnet" "ru-central1-b" {
  name           = "ru-central1-b subnet"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.main.id
  v4_cidr_blocks = [var.vpc_cidr_b]
}