output "id" {
    value = yandex_vpc_network.main.id
}

output "ru-central1-a-id" {
    value = yandex_vpc_subnet.ru-central1-a.id
}

output "ru-central1-b-id" {
    value = yandex_vpc_subnet.ru-central1-b.id
}