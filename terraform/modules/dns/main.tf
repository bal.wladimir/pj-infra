terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
}

resource "yandex_dns_zone" "public_zone" {
  name        = "public-testsalad-tk-zone"
  description = "public zone for testalad.tk"

  labels = {
    label1 = "public"
  }

  zone   = "testsalad.tk."
  public = true
}